const webpack = require('webpack');
const path = require('path');
const _ = require('lodash');
var HtmlWebpackPlugin = require('html-webpack-plugin');

const __ROOT = path.resolve(__dirname);

const defaultConfig = {
    entry: {
        main: './client/main.js'
    },
    output: {
        path: path.join(__ROOT, 'build'),
        filename: '[name].js',
    },

    resolve: {
        extensions: ['', '.js', '.scss', '.html'],
    },

    module: {
        loaders: [{
            test: /\.js$/i,
            loaders: ['babel'],
            exclude: /node_modules/i
        }, {
            test: /\.html$/i,
            loaders: ['html?attrs[]=img:src']
        }, {
            test: /\.scss$/i,
            loaders: ['style', 'css', 'sass']
        }, {
            test: /\.(png|jpg|gif)$/,
            loader: 'url?limit=25000'
        }, {
            test: /\.(woff|woff2?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url?limit=25000'
        },],
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'DesignStudio',
            template: './client/index.html'
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
    ]
}


module.exports = function (options) {
    return _.defaultsDeep(defaultConfig, options);
}


//
// const webpack = require('webpack');
//
// module.exports = {
//     entry: {
//         main: './client/main.js'
//     },
//     output: {
//         path: (__dirname + '/client'),
//         filename: 'bundle.js',
//     },
//
//     resolve: {
//         extensions: ['', '.js', '.scss', '.html'],
//     },
//
//     module: {
//         loaders: [{
//             test: /\.js$/i,
//             loaders: ['babel'],
//             exclude: /node_modules/i
//         }, {
//             test: /\.html$/i,
//             loaders: ['html?attrs[]=img:src']
//         }, {
//             test: /\.scss$/i,
//             loaders: ['style', 'css', 'sass']
//         }, {
//             test: /\.(png|jpe?g|gif)$/i,
//             loader: 'url'
//         }, {
//             test: /\.(woff|woff2?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
//             loader: 'url?limit=25000'
//         },],
//     },
//     plugins: [
//         new webpack.ProvidePlugin({
//             $: "jquery",
//             jQuery: "jquery",
//             "window.jQuery": "jquery"
//         }),
//     ]
// }
//
//
