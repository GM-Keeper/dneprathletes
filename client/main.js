import 'bootstrap';
import angular from 'angular';
import 'angular-ui-router';
require('font-awesome/scss/font-awesome.scss');


import './components/core';

angular.module('DneprAthletes', [
    'app.core',
])
    .config(configureModule);

configureModule.$inject = ['$urlRouterProvider', '$locationProvider'];

function configureModule($urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
}
