socialBtn.$inject = [];

function socialBtn() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        template: require('../layouts/social.btn.html'),
        bindToController: {},
        controller: socialBtnCtrl,
        controllerAs: 'mh'
    }
};

socialBtnCtrl.$inject = [];


function socialBtnCtrl() {
    console.log("Social_btn_group -- include!");
}

export default socialBtn;