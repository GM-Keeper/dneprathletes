mainMenu.$inject = [];

function mainMenu() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        template: require('../layouts/main.menu.html'),
        bindToController: {},
        controller: menuCtrl,
        controllerAs: 'mc'
    }
};

menuCtrl.$inject = [];


function menuCtrl() {
    console.log("Main_menu -- include!");
}

export default mainMenu;