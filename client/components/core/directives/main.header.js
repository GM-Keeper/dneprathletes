mainHeader.$inject = [];

function mainHeader() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        template: require('../layouts/main.header.html'),
        bindToController: {},
        controller: headerCtrl,
        controllerAs: 'mh'
    }
};

headerCtrl.$inject = [];


function headerCtrl() {
    console.log("Main_header -- include!");
}

export default mainHeader;