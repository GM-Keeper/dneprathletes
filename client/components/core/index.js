import angular from 'angular';
import './resources/scss/core.style.scss';
import mainHeader from './directives/main.header';
import socialBtn from './directives/social.btn';
import mainMenu from './directives/main.menu';
import '../home';

angular.module('app.core', [
    'ui.router',
    'app.home',
])
    .config(configureModule)
    .directive('mainHeader', mainHeader)
    .directive('socialBtn', socialBtn)
    .directive('mainMenu', mainMenu)

configureModule.$inject = ['$stateProvider'];

function configureModule($stateProvider) {
    $stateProvider
        .state('core', {
            template: require('./layouts/index.html'),
            abstract: true,
        });
};