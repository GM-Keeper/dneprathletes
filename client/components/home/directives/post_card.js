postcard.$inject = [];


function postcard() {
    return {
        restrict: 'E',
        transclude: true,
        // scope: {},
        template: require('../layouts/post_card.html'),
        bindToController: {},
        controller: postCtrl,
        controllerAs: 'pc',
    }
};

postCtrl.$inject = [];


function postCtrl() {
    console.log('Post_card -- include');
}


export default postcard;