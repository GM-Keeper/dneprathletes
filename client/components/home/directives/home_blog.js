homeBlog.$inject = [];


function homeBlog() {
    return {
        restrict: 'E',
        transclude: true,
        // scope: {},
        template: require('../layouts/home_blog.html'),
        bindToController: {},
        controller: hblogCtrl,
        controllerAs: 'hb'
    }
}

hblogCtrl.$inject = ["$scope"];

function hblogCtrl($scope) {

    $scope.tst = [
        {title: "Test 1"},
        {title: "Test 2"},
        {title: "Test 3"}
        ];
    console.log('Home_blog  -- include');
}

export default homeBlog;