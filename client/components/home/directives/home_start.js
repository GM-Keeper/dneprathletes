homeStart.$inject = [];

function homeStart() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        template: require('../layouts/home_start.html'),
        bindToController: {},
        controller: hstartCtrl,
        controllerAs: 'hs',
    }
};

hstartCtrl.$inject = [];

function hstartCtrl() {
    console.log("Home_start_block -- include");
}

export default homeStart;