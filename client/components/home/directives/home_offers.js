"use strict";
offersBlock.$inject = [];


function offersBlock() {
    return {
        restrict: 'E',
        transclude: true,
        template: require('../layouts/home_offers.html'),
        bindToController: {},
        controller: offersCtrl,
        controllerAs: 'offer',
    }
}


offersCtrl.$inject = [];

function offersCtrl() {
    console.log("Offers block -- include");
}

export default offersBlock;