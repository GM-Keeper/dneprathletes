import angular from 'angular';
import './resources/scss/home.style.scss';
import homeStart from './directives/home_start';
import homeBlog from './directives/home_blog';
import postcard from './directives/post_card';
import offersBlock from './directives/home_offers';

angular.module('app.home', ['app.core'])
    .config(configureModule)
    .directive('homeStart', homeStart)
    .directive('homeBlog', homeBlog)
    .directive('postCard', postcard)
    .directive('offersBlock', offersBlock)

configureModule.$inject = ['$stateProvider'];

function configureModule($stateProvider) {
    $stateProvider
        .state('core.home', {
            url: '/',
            template: require('./layouts/index.html'),
        });

}
