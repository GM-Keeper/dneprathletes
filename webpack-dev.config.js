const webpack = require('webpack');

const devConfig = require('./webpack.config.js')({
    devServer: {
        contentBase: './build',
    },
    module: {
        postLoaders: [],
    },

    devtool: 'eval'
})

devConfig.plugins.push(new webpack.HotModuleReplacementPlugin());

module.exports = devConfig;
