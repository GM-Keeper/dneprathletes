var express = require('express');
var app = express();
var fs = require('fs');


app.use(express.static('client'));

app.get('/', function (req, res) {
    var content = fs.readFileSync(__dirname + '/client/index.html');
    res.send(content);
});


var port = 9000;
app.listen(port);
console.log('Listening on port', port);